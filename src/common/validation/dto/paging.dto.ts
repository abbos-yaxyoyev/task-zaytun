import { Expose, Transform } from "class-transformer";
import { IsBoolean, IsNumber, IsOptional, IsPhoneNumber, IsString } from "class-validator";
import { CommonDto, CommonDtoGroup } from "./common.dto";

export class PagingDto extends CommonDto {
  @IsOptional({ groups: [CommonDtoGroup.PAGENATION] })
  @Transform(({ value }) => `+${value?.replace(/[^0-9]/g, "")}`)
  @IsPhoneNumber('UZ', { groups: [CommonDtoGroup.PAGENATION] })
  phone: string;

  @Transform(({ value }) => Number(value))
  @IsNumber(
    {
      allowInfinity: false,
      allowNaN: false,
      maxDecimalPlaces: 0,
    },
    {
      groups: [CommonDtoGroup.PAGENATION],
    }
  )
  limit!: number;

  @Transform(({ value }) => Number(value))
  @IsNumber(
    {
      allowInfinity: false,
      allowNaN: false,
      maxDecimalPlaces: 0,
    },
    {
      groups: [CommonDtoGroup.PAGENATION],
    }
  )
  page!: number;

  @Expose({ toClassOnly: true })
  @Transform(({ value }) => value?.trim() || "")
  @IsOptional({
    groups: [CommonDtoGroup.PAGENATION],
  })
  @IsString({
    groups: [CommonDtoGroup.PAGENATION],
  })
  search?: string;

  @IsOptional({ groups: [CommonDtoGroup.PAGENATION] })
  @IsString({ groups: [CommonDtoGroup.PAGENATION] })
  orderBy?: string;

  @IsOptional({ groups: [CommonDtoGroup.PAGENATION] })
  @Transform(({ value }) => value == true, { groups: [CommonDtoGroup.PAGENATION] })
  @IsBoolean({ groups: [CommonDtoGroup.PAGENATION] })
  isAsc?: boolean;
}
