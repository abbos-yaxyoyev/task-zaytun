import FastifyCors from '@fastify/cors';
import fastify from 'fastify';
import 'reflect-metadata';
import { replyDecorator } from '../common/decorators/reply.decorator';
import { ENV } from './../common/config/config';

//! plugin
import { routesPlugin } from './index.route';

const server = fastify({ logger: true });

server.register(FastifyCors, { origin: true });

//! plugin
server.register(replyDecorator);
server.register(routesPlugin);

async function start() {
  try {
    const options = {
      host: ENV.HOST,
      port: ENV.EMPLOYEE_PORT,
    };
    await server.listen(options);
    server.log.info(server.route);
    server.log.info('Started server successfully');
  } catch (error) {
    process.exit(1);
  }
}

start();
