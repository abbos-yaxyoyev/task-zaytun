import { Transform } from "class-transformer";
import { IsOptional, IsPhoneNumber, IsString } from "class-validator";
import { CommonDto, CommonDtoGroup } from "../../common/validation/dto/common.dto";


export class UserDtoGroup extends CommonDtoGroup {
  static readonly LOGIN = "login"
}

export class UserDto extends CommonDto {

  @IsOptional({ groups: [UserDtoGroup.UPDATE,] })
  @IsString({ groups: [UserDtoGroup.CREATE, UserDtoGroup.UPDATE] })
  fullName: string;

  @IsOptional({ groups: [UserDtoGroup.UPDATE] })
  @Transform(({ value }) => `+${value?.replace(/[^0-9]/g, '')}`)
  @IsPhoneNumber("UZ", { groups: [UserDtoGroup.CREATE, UserDtoGroup.UPDATE, UserDtoGroup.LOGIN] })
  phone: string;

  @IsOptional({ groups: [UserDtoGroup.UPDATE,] })
  @IsString({ groups: [UserDtoGroup.CREATE, UserDtoGroup.UPDATE, UserDtoGroup.LOGIN, UserDtoGroup.SET_PASSWORD] })
  password: string;

}
