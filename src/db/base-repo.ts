import { Client, Pool } from "pg";
import { PagingDto } from "../common/validation/dto/paging.dto";
import { ENV } from './../common/config/config';
import { IBaseRepository } from "./base-repo-interface";
import { insertParams } from "./utilities/insert-params";
import { objListToCamel, objToCamel } from "./utilities/object-transformator";
import { updateParams } from "./utilities/update-params";


export class BaseRepository<T> implements IBaseRepository<T>{

  protected readonly pool = new Pool(ENV.DB);

  constructor(
    protected readonly tableName: string,
  ) { }

  async count(where, values?: []): Promise<number> {
    const query =
      "SELECT " + `COUNT(*)` + " FROM " + this.tableName + " WHERE " + where;

    return (await this.pool.query(query, values)).rows[0].count;
  }

  async create(data: T): Promise<T> {
    const params = insertParams(data);
    const query = `INSERT INTO ${this.tableName} (${params.fields.join(
      ",",
    )}) VALUES (${params.variables.join(",")}) RETURNING *;`;

    const result = await this.pool.query(query, params.values);
    return objToCamel<T>(result.rows[0]);
  }

  async updateById(id: number, data: T): Promise<T> {
    const params = updateParams(data);
    const query = `UPDATE ${this.tableName} SET ${params.fields.join(
      ",",
    )} WHERE is_deleted = FALSE AND id = ${id} RETURNING *;`;

    const result = await this.pool.query(query, params.values);
    return objToCamel<T>(result.rows[0]);
  }

  async deleteById(id: number): Promise<void> {
    const query = `
    UPDATE ${this.tableName} 
    SET is_deleted = $1  
    WHERE id = ${id} AND is_deleted = FALSE RETURNING *;
    `;

    await this.pool.query(query, [true]);
  }

  async getById(id: number): Promise<T> {
    const query = `
      SELECT *
      FROM ${this.tableName}
      WHERE id = $1 AND is_deleted = FALSE;
    `;

    const result = await this.pool.query(query, [id]);
    return objToCamel<T>(result.rows[0]);
  }

  public async paging(
    dto: PagingDto,
    fields: string[],
    where: string
  ): Promise<{ total: number, data: T[] }> {


    if (dto.createdAt) where += `AND createdAt = ${dto.createdAt}`;

    where += `  OFFSET ${(1 * dto.page - 1) * dto.limit} LIMIT ${dto.limit}`;
    const query = `SELECT ${fields.join(",")} FROM ${this.tableName} WHERE ${where};`;

    const [total, data] = await Promise.all([
      this.count(where),
      this.pool.query<any>(query)
    ])

    const result = objListToCamel<T>(data.rows);


    return { total, data: result };
  }
}

export const withTransaction = async (callback) => {

  const client = new Client(ENV.DB);

  try {
    await client.query('BEGIN');
    await callback(client);
    await client.query('COMMIT');
  } catch (error) {
    await client.query('ROLLBACK');
    throw error;
  } finally {
    client.end();
  }
};


