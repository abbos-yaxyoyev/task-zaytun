import jwt from 'jsonwebtoken';
import { ENV } from '../config/config';
import { BaseError } from '../errors/base.error';
import { userService } from './../../domain/user/service';

export async function authUser(request, reply) {
  const token = request.headers.authorization?.split(' ')[1];
  if (!token) {
    return reply.status(401).send(BaseError.UnAuthorizationError());
  }
  try {
    const { id, } = jwt.verify(token, ENV.JWT_SECRET) as { id: number };
    const user = await userService.getById(id);
    request.user = user;
  } catch (error) {
    return reply.status(401).send(BaseError.UnAuthorizationError());
  }
}
