import { stringToSnake } from "./string-transformator";

export function insertParams(obj) {
  const fields: string[] = [];
  const variables: any[] = [];
  const values: any[] = [];
  let indx = 1;

  for (const [key, value] of Object.entries(obj)) {
    const field = stringToSnake(key);
    fields.push(field);
    variables.push(`$${indx}`);
    values.push(value);
    indx++;
  }

  return {
    fields,
    variables,
    values
  }
}