import { IBaseRepository } from "../../db/base-repo-interface";

export interface IMovieRepository<T> extends IBaseRepository<T> {
  getPaging(data): Promise<{ total: number, data: T[] }>;
}