import md5 from 'md5';
import { BaseError } from '../../common/errors/base.error';
import { signAsync } from '../../common/plugin/authPlugin';
import { PagingDto } from '../../common/validation/dto/paging.dto';
import { EmployeeEntity } from './entity';
import { EmployeeRepository } from './repository';
import { IEmployeeRepository } from './repository-interface';

class EmployeeService {
  constructor(
    private readonly employeeRepository: IEmployeeRepository<EmployeeEntity>
  ) { }

  async register(data) {
    data.password = md5(data.password)
    const result = await this.employeeRepository.create(data);
    return result;
  }

  async login(request, data) {
    const result = await this.employeeRepository.login(data.phone);
    if (!result) throw BaseError.NotFound(data.phone);
    if (result.password != md5(data.password)) throw BaseError.IncorrectPassword();
    const token = await signAsync({ id: result.id });
    return { token, user: result };
  }

  async setPassword(data) {
    data.password = md5(data.password)
    const result = await this.employeeRepository.setPassword(data.id, data.password);
    return result;
  }

  async getById(id: number) {
    const result = await this.employeeRepository.getById(id);
    return result;
  }

  async getPaging(dto: PagingDto) {
    const result = await this.employeeRepository.getPaging(dto);
    return result;
  }

  async updateById(id: number, data) {
    if (data.password) delete data.password;
    const result = await this.employeeRepository.updateById(id, data);
    return result;
  }

  async deleteById(id: number) {
    const result = await this.employeeRepository.deleteById(id);
    return result;
  }

}

export const employeeService = new EmployeeService(new EmployeeRepository())