import { stringToCamel, stringToSnake } from "./string-transformator";

export function objToSnake(obj) {
  let res = {};

  for (const key of Object.keys(obj)) {
    res[stringToSnake(key)] = obj[key];
  }

  return res;
}

export function objListToSnake(obj: any[]) {
  return obj.map((o) => objToSnake(o));
}

export function objToCamel<T>(obj) {
  let res: any = {};

  for (const key of Object.keys(obj)) {
    res[stringToCamel(key)] = obj[key];
  }

  return res;
}

export function objListToCamel<T>(obj: any[]) {
  return obj.map((o) => objToCamel<T>(o));
}
