import { authUser } from "../../common/middleware/user.auth";
import { deleteAccount, getById, login, register, setPassword, updateById } from "./controller";

export const userRoutes = [
  {
    method: 'POST',
    url: `/user/login`,
    handler: login,
  },
  {
    method: 'POST',
    url: `/user`,
    handler: register,
  },
  {
    method: 'PUT',
    url: `/user`,
    preValidation: [authUser],
    handler: updateById,
  },
  {
    method: 'PUT',
    url: `/user/password`,
    preValidation: [authUser],
    handler: setPassword,
  },
  {
    method: 'GET',
    url: `/user/:id`,
    // preValidation: [authUser],
    handler: getById,
  },
  {
    method: 'DELETE',
    url: `/user/:id`,
    preValidation: [authUser],
    handler: deleteAccount,
  }
];
