CREATE TABLE "employees" (
  "id" BIGSERIAL NOT NULL PRIMARY KEY,
  "full_name" VARCHAR(255),
  "phone" VARCHAR(255),
  "password" VARCHAR(255),
  "is_deleted" BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE UNIQUE INDEX phone_number_employee ON employees (phone)
WHERE
  is_deleted = false;

CREATE TABLE "users" (
  "id" BIGSERIAL NOT NULL PRIMARY KEY,
  "full_name" VARCHAR(255),
  "phone" VARCHAR(255),
  "password" VARCHAR(255),
  "is_deleted" BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE UNIQUE INDEX phone_number_user ON users (phone)
WHERE
  is_deleted = false;

CREATE TABLE "movies" (
  "id" BIGSERIAL NOT NULL PRIMARY KEY,
  "duration" FLOAT NOT NULL,
  "title" VARCHAR(255),
  "description" VARCHAR(255),
  "is_deleted" BOOLEAN NOT NULL DEFAULT FALSE
);