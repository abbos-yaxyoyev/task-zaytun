import { authEmployee } from "../../common/middleware/employee.auth";
import { create, deleted, getById, getPaging, updateById } from "./controller";

export const movieRoutes = [
  {
    method: 'POST',
    url: `/movie`,
    preValidation: [authEmployee],
    handler: create,
  },
  {
    method: 'PUT',
    url: `/movie`,
    preValidation: [authEmployee],
    handler: updateById,
  },
  {
    method: 'DELETE',
    url: `/movie/:id`,
    preValidation: [authEmployee],
    handler: deleted,
  },
  {
    method: 'GET',
    url: `/movie/:id`,
    handler: getById,
  },
  {
    method: 'GET',
    url: `/movie`,
    handler: getPaging,
  }
];
