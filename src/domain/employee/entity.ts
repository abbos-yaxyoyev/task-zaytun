import { BaseEntity } from "../../db/base-entity";

export class EmployeeEntity extends BaseEntity {
  public fullName: string;
  public phone: string; // unique
  public password: string;

  constructor(
    fullName: string,
    phone: string,
    password: string,
  ) {
    super();
    this.fullName = fullName;
    this.phone = phone;
    this.password = password;
  }

}