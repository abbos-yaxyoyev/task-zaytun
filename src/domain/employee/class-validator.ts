import { Transform } from "class-transformer";
import { IsOptional, IsPhoneNumber, IsString } from "class-validator";
import { CommonDto, CommonDtoGroup } from "../../common/validation/dto/common.dto";


export class EmployeeeDtoGroup extends CommonDtoGroup {
  static readonly LOGIN = "login"
}

export class EmployeeeDto extends CommonDto {

  @IsOptional({ groups: [EmployeeeDtoGroup.UPDATE,] })
  @IsString({ groups: [EmployeeeDtoGroup.CREATE, EmployeeeDtoGroup.UPDATE] })
  fullName: string;

  @IsOptional({ groups: [EmployeeeDtoGroup.UPDATE] })
  @Transform(({ value }) => `+${value?.replace(/[^0-9]/g, '')}`)
  @IsPhoneNumber("UZ", { groups: [EmployeeeDtoGroup.CREATE, EmployeeeDtoGroup.UPDATE, EmployeeeDtoGroup.LOGIN] })
  phone: string;

  @IsOptional({ groups: [EmployeeeDtoGroup.UPDATE,] })
  @IsString({ groups: [EmployeeeDtoGroup.CREATE, EmployeeeDtoGroup.UPDATE, EmployeeeDtoGroup.LOGIN] })
  password: string;

}
