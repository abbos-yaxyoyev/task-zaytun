import { table } from "../../common/constants/tables";
import { PagingDto } from "../../common/validation/dto/paging.dto";
import { BaseRepository } from "../../db/base-repo";
import { objToCamel } from '../../db/utilities/object-transformator';
import { EmployeeEntity } from './entity';
import { IEmployeeRepository } from './repository-interface';


export class EmployeeRepository extends BaseRepository<EmployeeEntity>
  implements IEmployeeRepository<EmployeeEntity>
{
  constructor() {
    super(table.employees);
  }

  async setPassword(id: string, password: string): Promise<EmployeeEntity> {

    const query = `
      UPDATE ${this.tableName}
      SET
        password=$2
      WHERE id = $1 AND is_deleted = FALSE
      RETURNING *
    `;

    const result = await this.pool.query(query, [id, password]);
    return objToCamel<EmployeeEntity>(result.rows[0]);

  }

  public async login(phone: string): Promise<EmployeeEntity> {
    const fields: string[] = [];

    fields.push('id');
    fields.push('full_name');
    fields.push('phone');

    const query = `
    SELECT *
    FROM ${this.tableName}
    WHERE phone = $1 AND is_deleted = FALSE 
  `;
    const result = await this.pool.query(query, [phone]);
    return objToCamel<EmployeeEntity>(result.rows[0]);
  }

  public async getPaging(dto: PagingDto): Promise<{ total: number, data: EmployeeEntity[] }> {

    const fields: string[] = [];

    fields.push('id');
    fields.push('full_name');
    fields.push('phone');

    let where = 'is_deleted = FALSE';
    if (dto.search) where += `AND (phone1 || phone2 || first-name || last_name ) ILIKE %${dto.search}%`;

    const result = await this.paging(dto, fields, where);
    return result;

  }



}
