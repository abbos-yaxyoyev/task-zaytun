import { PagingDto } from '../../common/validation/dto/paging.dto';
import { validateIt } from './../../common/validation/validate';
import { EmployeeeDto, EmployeeeDtoGroup } from './class-validator';
import { employeeService } from './service';

export async function register(request, response) {

  const data = await validateIt(request.body, EmployeeeDto, [EmployeeeDtoGroup.CREATE]);

  const result = await employeeService.register(data);
  return response.success(result);

}

export async function login(request, response) {

  const data = await validateIt(request.body, EmployeeeDto, [EmployeeeDtoGroup.LOGIN]);

  const result = await employeeService.login(request, data);
  return response.success(result);

}

export async function updateById(request, response) {

  const data = await validateIt(request.body, EmployeeeDto, [EmployeeeDtoGroup.UPDATE]);

  const result = await employeeService.updateById(data.id, data);
  return response.success(result);

}

export async function setPassword(request, response) {

  const data = await validateIt(request.body, EmployeeeDto, [EmployeeeDtoGroup.SET_PASSWORD]);

  const result = await employeeService.updateById(data.id, data);
  return response.success(result);

}

export async function deleteAccount(request, response) {

  const data = await validateIt(request.params, EmployeeeDto, [EmployeeeDtoGroup.DELETE]);

  const result = await employeeService.deleteById(data.id);
  return response.success(result);

}

export async function getById(request, response) {

  const data = await validateIt(request.params, EmployeeeDto, [EmployeeeDtoGroup.GET_BY_ID]);

  const result = await employeeService.getById(data.id);
  return response.success(result);

}

export async function getPagingEmployee(request, response) {

  const data = await validateIt(request.query, PagingDto, [EmployeeeDtoGroup.PAGENATION]);

  const result = await employeeService.getPaging(data);
  return response.success(result);

}