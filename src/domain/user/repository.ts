import { table } from "../../common/constants/tables";
import { PagingDto } from "../../common/validation/dto/paging.dto";
import { BaseRepository } from "../../db/base-repo";
import { objToCamel } from "../../db/utilities/object-transformator";
import { UserEntity } from './entity';
import { IUserRepository } from './repository-interface';


export class UserRepository extends BaseRepository<UserEntity>
  implements IUserRepository<UserEntity>
{
  constructor() {
    super(table.user);
  }

  public async login(phone: string): Promise<UserEntity> {
    const fields: string[] = [];

    fields.push('id');
    fields.push('full_name');
    fields.push('phone');

    const query = `
    SELECT *
    FROM ${this.tableName}
    WHERE phone = $1 AND is_deleted = FALSE 
  `;
    const result = await this.pool.query(query, [phone]);
    return objToCamel<UserEntity>(result.rows[0]);
  }

  async setPassword(id: number, password: string): Promise<UserEntity> {

    const query = `
      UPDATE ${this.tableName}
      SET
        password=$2
      WHERE id = $1 AND is_deleted = FALSE
      RETURNING *
    `;

    const result = await this.pool.query(query, [id, password]);
    return objToCamel<UserEntity>(result.rows[0]);

  }

  public async getPaging(dto: PagingDto): Promise<{ total: number, data: UserEntity[] }> {

    const fields: string[] = [];

    fields.push('id');
    fields.push('full_name');
    fields.push('phone');

    let where = 'is_deleted = FALSE';
    if (dto.search) where += `AND (phone || full_name) ILIKE %${dto.search}%`;

    const result = await this.paging(dto, fields, where);
    return result;

  }



}
