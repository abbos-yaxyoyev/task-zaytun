import fp from 'fastify-plugin';
import { BaseError } from '../errors/base.error';

async function pl(instance, options, next) {
  instance.decorateReply('success', function (result: any = 'ok') {
    this.status(200).send(BaseError.Success(result));
  });

  // global error handler
  instance.setErrorHandler((error, request, reply) => {
    console.log(error)
    let response;
    if (error instanceof BaseError) {
      response = error;
    } else {
      response = BaseError.UnknownError(error)
    }

    reply.status(400).send(response)
  });

  next();
}

export const replyDecorator = fp(pl);
