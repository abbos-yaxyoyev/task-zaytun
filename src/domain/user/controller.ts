import { PagingDto } from '../../common/validation/dto/paging.dto';
import { validateIt } from './../../common/validation/validate';
import { UserDto, UserDtoGroup } from './class-validator';
import { userService } from './service';

export async function register(request, response) {

  const data = await validateIt(request.body, UserDto, [UserDtoGroup.CREATE]);

  const result = await userService.register(data);
  return response.success(result);

}

export async function login(request, response) {

  const data = await validateIt(request.body, UserDto, [UserDtoGroup.LOGIN]);

  const result = await userService.login(data);
  return response.success(result);

}

export async function updateById(request, response) {

  const data = await validateIt(request.body, UserDto, [UserDtoGroup.UPDATE]);

  const result = await userService.updateById(data.id, data);
  return response.success(result);

}

export async function setPassword(request, response) {

  const data = await validateIt(request.body, UserDto, [UserDtoGroup.SET_PASSWORD]);

  const result = await userService.setPassword(data);
  return response.success(result);

}

export async function deleteAccount(request, response) {

  const data = await validateIt(request.params, UserDto, [UserDtoGroup.DELETE]);

  const result = await userService.deleteById(data.id);
  return response.success(result);

}

export async function getById(request, response) {
  const data = await validateIt(request.params, UserDto, [UserDtoGroup.GET_BY_ID]);

  const result = await userService.getById(data.id);
  return response.success(result);

}

export async function getPaging(request, response) {

  const data = await validateIt(request.query, PagingDto, [UserDtoGroup.PAGENATION]);

  const result = await userService.getPaging(data);
  return response.success(result);

}