export interface IBaseRepository<T> {
  count(where, values?): Promise<number>;
  create(data: T): Promise<T>;
  updateById(id: number, data: T): Promise<T>;
  deleteById(id: number): Promise<void>;
  getById(id: number): Promise<T>;
  paging(dto, fields: string[], where: string): Promise<{ total: number, data: T[] }>;
}