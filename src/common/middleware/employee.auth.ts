import jwt from 'jsonwebtoken';
import { ENV } from '../config/config';
import { BaseError } from '../errors/base.error';
import { employeeService } from './../../domain/employee/service';

export async function authEmployee(request, reply) {
  try {
    const token = request.headers.authorization?.split(' ')[1];
    if (!token) {
      return reply.status(401).send(BaseError.UnAuthorizationError());
    }
    const { id, } = jwt.verify(token, ENV.JWT_SECRET) as { id: number };
    const employee = await employeeService.getById(id);
    request.employee = employee;
  } catch (error) {
    return reply.status(401).send(BaseError.UnAuthorizationError());
  }
}
