import md5 from 'md5';
import { BaseError } from '../../common/errors/base.error';
import { signAsync } from '../../common/plugin/authPlugin';
import { PagingDto } from '../../common/validation/dto/paging.dto';
import { UserEntity } from './entity';
import { UserRepository } from './repository';
import { IUserRepository } from './repository-interface';
class UserService {
  constructor(
    private readonly userRepository: IUserRepository<UserEntity>
  ) { }

  async register(data) {
    data.password = md5(data.password)
    const result = await this.userRepository.create(data);
    return result;
  }

  async login(data) {
    const result = await this.userRepository.login(data.phone);
    if (!result) throw BaseError.NotFound(data.phone);
    if (result.password != md5(data.password)) throw BaseError.IncorrectPassword();
    const token = await signAsync({ id: result.id });
    return { token, user: result };
  }

  async setPassword(data) {
    data.password = md5(data.password)
    const result = await this.userRepository.setPassword(data.id, data.password);
    return result;
  }

  async getById(id: number) {
    const result = await this.userRepository.getById(id);
    return result;
  }

  async getPaging(dto: PagingDto) {
    const result = await this.userRepository.getPaging(dto);
    return result;
  }

  async updateById(id: number, data) {
    if (data.password) delete data.password;
    const result = await this.userRepository.updateById(id, data);
    return result;
  }

  async deleteById(id: number) {
    const result = await this.userRepository.deleteById(id);
    return result;
  }

}

export const userService = new UserService(new UserRepository())