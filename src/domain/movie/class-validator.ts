import { IsNumber, IsOptional, IsString } from "class-validator";
import { CommonDto, CommonDtoGroup } from "../../common/validation/dto/common.dto";


export class MovieDtoGroup extends CommonDtoGroup { }

export class MovieDto extends CommonDto {

  @IsOptional({ groups: [MovieDtoGroup.UPDATE,] })
  @IsString({ groups: [MovieDtoGroup.CREATE, MovieDtoGroup.UPDATE] })
  title: string;

  @IsOptional({ groups: [MovieDtoGroup.UPDATE,] })
  @IsString({ groups: [MovieDtoGroup.CREATE, MovieDtoGroup.UPDATE] })
  description: string;

  @IsOptional({ groups: [MovieDtoGroup.UPDATE,] })
  @IsNumber({ allowInfinity: false, allowNaN: false, }, { groups: [MovieDtoGroup.CREATE, MovieDtoGroup.UPDATE] })
  duration: number;

}
