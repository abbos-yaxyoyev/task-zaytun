import { authEmployee } from "../../common/middleware/employee.auth";
import { getPaging } from "../user/controller";
import { deleteAccount, getById, getPagingEmployee, login, register, setPassword, updateById } from "./controller";

export const employeeRoutes = [
  {
    method: 'POST',
    url: `/employee/login`,
    handler: login,
  },
  {
    method: 'POST',
    url: `/employee`,
    handler: register,
  },
  {
    method: 'PUT',
    url: `/employee`,
    preValidation: [authEmployee],
    handler: updateById,
  },
  {
    method: 'PUT',
    url: `/employee/password`,
    preValidation: [authEmployee],
    handler: setPassword,
  },
  {
    method: 'GET',
    url: `/employee/:id`,
    preValidation: [authEmployee],
    handler: getById
  },
  {
    method: 'GET',
    url: `/employee`,
    preValidation: [authEmployee],
    handler: getPagingEmployee,
  },
  {
    method: 'DELETE',
    url: `/employee/:id`,
    preValidation: [authEmployee],
    handler: deleteAccount,
  },
  //user get paging
  {
    method: 'GET',
    url: `/employee/user`,
    // preValidation: [authEmployee],
    handler: getPaging,
  }
];
