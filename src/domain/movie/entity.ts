import { BaseEntity } from "../../db/base-entity";

export class MovieEntity extends BaseEntity {

  public duration: number;
  public title: string;
  public description: string;

  constructor(
    duration: number,
    title: string,
    description: string
  ) {
    super();
    this.duration = duration;
    this.title = title;
    this.description = description;
  }

}