import { table } from "../../common/constants/tables";
import { PagingDto } from "../../common/validation/dto/paging.dto";
import { BaseRepository } from "../../db/base-repo";
import { MovieEntity } from './entity';
import { IMovieRepository } from './repository-interface';


export class MovieRepository extends BaseRepository<MovieEntity>
  implements IMovieRepository<MovieEntity>
{
  constructor() {
    super(table.movie);
  }

  public async getPaging(dto: PagingDto): Promise<{ total: number, data: MovieEntity[] }> {

    const fields: string[] = [];

    fields.push('id');
    fields.push('title');
    fields.push('duration');
    fields.push('description');

    let where = 'is_deleted = FALSE';
    if (dto.search) where += `AND title ILIKE %${dto.search}%`;

    const result = await this.paging(dto, fields, where);
    return result;

  }

}
