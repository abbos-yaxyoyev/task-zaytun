export const ERROR_CODES = {
  BASE: 10000,
  USER: 12000,
  EMPLOYEE: 13000,
  MOVIE: 14000
};
