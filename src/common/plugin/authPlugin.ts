import jwt from 'jsonwebtoken';
import { ENV } from '../config/config';

const verifyJwt = (token: string): Promise<any> => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, ENV.JWT_SECRET, (err, decoded) => {
      if (err) {
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });
};

export function signAsync(payload: any, options?: jwt.SignOptions): Promise<string> {
  return new Promise((resolve, reject) => {
    jwt.sign(payload, ENV.JWT_SECRET, options, (err: any, token: string) => {
      if (err) {
        reject(err);
      } else {
        resolve(token);
      }
    });
  });
}
