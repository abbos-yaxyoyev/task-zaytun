import fp from 'fastify-plugin';
import { employeeRoutes } from './employee/routes';
import { movieRoutes } from './movie/routes';
import { userRoutes } from './user/routes';

const routes = [
  ...userRoutes,
  ...movieRoutes,
  ...employeeRoutes,

];

export async function pl(instance, _, next) {
  try {
    routes.map((route) => instance.route(route));
  } catch (error) {
    console.log(error);
    process.exit(1);
  }

  next();
}

export const routesPlugin = fp(pl);
