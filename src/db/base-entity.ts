export class BaseEntity {
  id: number;
  createdAt?: string;
  deletedAt?: string;
  isDeleted?: boolean;
}