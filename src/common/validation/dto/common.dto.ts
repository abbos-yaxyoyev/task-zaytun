import { Transform } from "class-transformer";
import { IsNumber, IsOptional } from "class-validator";

export class CommonDtoGroup {
  static readonly CREATE = "create";
  static readonly UPDATE = "update";
  static readonly DELETE = "delete";
  static readonly GET_BY_ID = "getById";
  static readonly PAGENATION = "pagination";
  static readonly SET_PASSWORD = "set-password";
}

export class CommonDto {
  @IsOptional({ groups: [CommonDtoGroup.PAGENATION] })
  @Transform(({ value }) => Number(value))
  @IsNumber({
    allowInfinity: false,
    allowNaN: false,
  }, {
    groups: [
      CommonDtoGroup.UPDATE,
      CommonDtoGroup.DELETE,
      CommonDtoGroup.GET_BY_ID,
      CommonDtoGroup.PAGENATION,
      CommonDtoGroup.SET_PASSWORD
    ],
  })
  id: number;

  @IsOptional({ groups: [CommonDtoGroup.DELETE] })
  isDeleted: boolean;

  createdAt?: Date;

}

export class GetPagingDto { }
