import { ERROR_CODES } from '../constants/errors';

export class BaseError {
  constructor(
    public code: number,
    public message: string,
    public data: any,
    public success: boolean = false,
    public statusCode: number = 400,
    public time = new Date()
  ) { }

  public static UnknownError(data?: any) {
    return new BaseError(ERROR_CODES.BASE, 'Unknown error', data);
  }

  public static ValidationError(data?: any) {
    return new BaseError(ERROR_CODES.BASE + 1, 'Validation Error', data);
  }

  public static Success(data: any = null) {
    return new BaseError(0, 'ok', data, true, 200)
  }

  public static UnAuthorizationError(data: any = null) {
    return new BaseError(401, 'session expired', data, false, 401)
  }

  public static IncorrectPassword(data: any = null) {
    return new BaseError(ERROR_CODES.BASE + 2, 'Invalid password or phone', data, false, 401)
  }

  public static NotFound(data: any = null) {
    return new BaseError(ERROR_CODES.BASE + 2, 'Not found', data, false, 401)
  }
}

