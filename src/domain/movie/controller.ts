import { PagingDto } from '../../common/validation/dto/paging.dto';
import { validateIt } from './../../common/validation/validate';
import { MovieDto, MovieDtoGroup } from './class-validator';
import { movieService } from './service';

export async function create(request, response) {

  const data = await validateIt(request.body, MovieDto, [MovieDtoGroup.CREATE]);

  const result = await movieService.create(data);
  return response.success(result);

}


export async function updateById(request, response) {

  const data = await validateIt(request.body, MovieDto, [MovieDtoGroup.UPDATE]);

  const result = await movieService.updateById(data.id, data);
  return response.success(result);

}

export async function deleted(request, response) {

  const data = await validateIt(request.params, MovieDto, [MovieDtoGroup.DELETE]);

  const result = await movieService.deleteById(data.id);
  return response.success(result);

}

export async function getById(request, response) {

  const data = await validateIt(request.params, MovieDto, [MovieDtoGroup.GET_BY_ID]);

  const result = await movieService.getById(data.id);
  return response.success(result);

}

export async function getPaging(request, response) {

  const data = await validateIt(request.query, PagingDto, [MovieDtoGroup.PAGENATION]);

  const result = await movieService.getPaging(data);
  return response.success(result);

}