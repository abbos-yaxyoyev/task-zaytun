import { PagingDto } from '../../common/validation/dto/paging.dto';
import { MovieEntity } from './entity';
import { MovieRepository } from './repository';
import { IMovieRepository } from './repository-interface';

class MovieService {
  constructor(
    private readonly movieRepository: IMovieRepository<MovieEntity>
  ) { }

  async create(data) {
    const result = await this.movieRepository.create(data);
    return result;
  }

  async getById(id: number) {
    const result = await this.movieRepository.getById(id);
    return result;
  }

  async getPaging(dto: PagingDto) {
    const result = await this.movieRepository.getPaging(dto);
    return result;
  }

  async updateById(id: number, data) {
    const result = await this.movieRepository.updateById(id, data);
    return result;
  }

  async deleteById(id: number) {
    const result = await this.movieRepository.deleteById(id);
    return result;
  }

}

export const movieService = new MovieService(new MovieRepository())