import { stringToSnake } from "./string-transformator";

export function updateParams(obj) {
  const fields: string[] = [];
  const values: any[] = [];
  let indx = 1;

  for (const [key, value] of Object.entries(obj)) {
    const field = stringToSnake(key);
    fields.push(field + `=$${indx}`);
    values.push(value);
    indx++;
  }

  return {
    fields,
    values
  }

}