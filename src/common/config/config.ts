export const ENV = {
  DB: {
    user: "postgres",
    host: "localhost",
    database: "taskzaytun",
    password: 'student',
    port: 5432,
  },
  HOST: process.env.HOST || '0.0.0.0',
  EMPLOYEE_PORT: 1030,
  JWT_SECRET: 'JWT_SECRET',
  JWT_EXPIRE: '1W',
};