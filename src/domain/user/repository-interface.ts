import { IBaseRepository } from "../../db/base-repo-interface";

export interface IUserRepository<T> extends IBaseRepository<T> {
  login(phone: string): Promise<T>;
  setPassword(id: number, password: string): Promise<T>;
  getPaging(data): Promise<{ total: number, data: T[] }>;
}