import { IBaseRepository } from "../../db/base-repo-interface";

export interface IEmployeeRepository<T> extends IBaseRepository<T> {
  login(phone: string): Promise<T>;
  getPaging(data): Promise<{ total: number, data: T[] }>;
  setPassword(id: string, password: string): Promise<T>;
}